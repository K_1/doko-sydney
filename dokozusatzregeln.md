# Zusatzregeln des 1. Doppelkopf Clubs Sydney

## 2. fängt 1. ♥10

Die zweite ♥10 übertrumpft die erste.

## Normalspiele

### Abgabe bei Armut

Hat ein Spieler außer ♦Assen weniger als 4 Trümpfe, so kann er in der
zweiten Vorbehaltsrunde, falls er eine ♣Dame hat, *Abgabe an reich*,
andernfalls *Abgabe an arm*, anmelden. Er gibt seine Trümpfe an seinen
Mitspieler ab, ggfs. nachdem die Hochzeit ausgespielt wurde, und
verkündet dabei, wieviele Karten abgegeben werden. Abgaben unter 2
Trümpfen müssen vom Mitspieler nicht angenommen werden. Der Mitspieler
gibt entsprechend viele Karten zurück. Er darf nur dann Trumpf
zurückgeben, wenn er keine Nicht-Trümpfe mehr hat. Bei Rückgabe
verkündet er die Anzahl der überreichten Trümpfe. Die Gegenpartei kann
bei Rückgabe von mehr als einem Trumpf das Spiel ablehnen.

### Schweine

Hat ein Spieler die beiden ♦Asse, so kann er sie im Normalspiel als
Vorbehalt in der 2. Runde als *Schweine* anmelden. Dies sind dann die
höchsten Trümpfe. ♦Asse, die erst durch [Abgabe bei Armut](#abgabe-bei-armut)
zusammengeführt werden, sind keine Schweine.

### Charly

Macht ein ♣Bube den letzten Stich eines Normalspiels, so wird dies durch
einen Sonderpunkt belohnt. Wird ein gegnerischer ♣Bube im letzten Stich
eines Normalspiels gefangen, so wird dies genauso belohnt. Wie bei allen
Karten (außer den ♥10en), so gilt auch hier: Erster schlägt Zweiten.

## Soli

### Vorführen

Nichtgespielte Pflichtsoli werden nach Ablauf der Regelspielzeit
vorgeführt, meist mit verheerendem Ausgang, es sei denn, Micha ist
Kontra.

### Gemischtes Solo

Nur die Damen und Buben sind Trumpf.

### Königensolo

Nur die Könige sind Trumpf.
