# 1. Doppelkopf Club Sydney

Gegründet am 17. September 1998.

## Regelwerk

Ausgangspunkt sind die *Turnier-Spielregeln Stand 01.04.2019* zu finden auf der Seite  [Regeln/Ordnungen](http://www.doko-verband.de/Regeln__Ordnungen.html) des Deutschen Doppelkopf-Verbands.

Dazu kommen ein paar [Zusatzregeln](dokozusatzregeln.md).

Diese Sonderregeln sind schnell erlernt und wir behalten uns das Recht vor, sie am Abend durch Mehrheitsbeschluss anwesender Stammspieler zu ändern. Auch auf der Südhalbkugel und auch bei Linksverkehr in Sydney spielen wir im Uhrzeigersinn.

Wir spielen um $`\textcolor{red}{\text{\textsf{\$AU0.1}}}`$ pro Punkt, was eigentlich irrelevant ist, weil die Kasse mittlerweile mehr Geld durch Strafen einnimmt.

## Strafenkatalog

(Strafen sind angegeben in $AU. Dazu kommen jeweils noch 10% GST):

1. Hochzeit, Doppeltulle und Schweine in einem Blatt: $`\textcolor{red}{\text{\textsf{\$5}}}`$
1. Telefonieren während des Spiels: $`\textcolor{red}{\text{\textsf{\$1}}}`$.
1. Unabsichtliches Bierverschütten: $`\textcolor{red}{\text{\textsf{\$1}}}`$
      (pro Einheit; 1 Einheit = 1 mal oder 1 Glas oder 1 Flasche).
1. Vorsätzliches Bierverschütten: $`\textcolor{red}{\text{\textsf{\$3}}}`$ (pro Einheit).
1. Vorwerfen mit Nachspiel: $`\textcolor{red}{\text{\textsf{\$1}}}`$ in einfachen Fällen oder aber bis zu $`\textcolor{red}{\text{\textsf{\$10}}}`$ je nach Beweislage und Dramatik des Effektes; die anwesenden Stammspieler entscheiden, allerdings gilt hier auch "Gnade vor Recht".
1. Spielverzögerung: per Beschluss je nach Spielstand und Situation: jedoch nicht mehr als $`\textcolor{red}{\text{\textsf{\$2}}}`$.
1. Vergeben:
   1. Unabsichtliches ~ per Beschluss von anwesenden
            Stammspielern je nach Spielstand und Situation: jedoch
            nicht mehr als $`\textcolor{red}{\text{\textsf{\$3}}}`$.
   1. Unabsichtliches mehrfaches direkt aufeinander folgendes ~ ist vorgekommen und wird als mehrfacher Fall von 7.1 abgehandelt; es gibt keine Rabatte.
   1. Vorsätzliches ~: ist noch nie vorgekommen und nicht nachweisbar => siehe 7.1-7.2.
1. Übergeben am Tisch: $`\textcolor{red}{\text{\textsf{\$10}}}`$ (plus eventuell anfallende Reinigungskosten).
1. Verzählen wenn Nachzählen beweist, dass der Spielausgang umgekehrt ist: $`\textcolor{red}{\text{\textsf{\$1}}}`$.
1. Unabsichtliches falsches Bedienen: $`\textcolor{red}{\text{\textsf{\$1}}}`$ in einfachen Fällen oder aber bis zu $`\textcolor{red}{\text{\textsf{\$10}}}`$ je nach Beweislage und Dramatik des Effektes, die anwesenden Stammspieler werden entscheiden, allerdings gilt hier auch "Gnade vor Recht".
1. Vorsätzliches falsches Bedienen ist noch nie vorgekommen, aber nachweisbar und wird mit $`\textcolor{red}{\text{\textsf{\$1}}}`$ in scherzhaften Fällen und mit Kreuzigung in maliziösen Fällen bestraft.
1. Unfähigkeit weiterzuspielen wegen übermässigen Alkoholgenusses. Meist folgt automatische Bestrafung durch hohe Spielverluste auf dem Weg in diesen Zustand. Hier gilt klar: "Gnade vor Recht".
1. Re oder Contra sagen obwohl kein Grund (kein gutes Blatt, kein außerordentlicher Spielverlauf) vorliegt:  bisher ohne Strafe (nur die ggfs. automatische Bestrafung durch Spielverlust und Extrapunkt(e) bei der Abrechung).
1. Keine 9 sagen obwohl kein Grund (kein gutes Blatt, kein außerordentlicher Spielverlauf) vorliegt:  bisher ohne Strafe (nur die ggfs. automatische Bestrafung durch Spielverlust und Extrapunkt(e) bei der Abrechung).
1. Keine 6 sagen obwohl kein Grund (kein gutes Blatt, kein außerordentlicher Spielverlauf) vorliegt:  bisher ohne Strafe (nur die ggfs. automatische Bestrafung durch Spielverlust und Extrapunkt(e) bei der Abrechung)
1. Keine 3 sagen obwohl kein Grund (kein gutes Blatt, kein außerordentlicher Spielverlauf) vorliegt:  bisher ohne Strafe (nur die ggfs. automatische Bestrafung durch Spielverlust und Extrapunkt(e) bei der Abrechung).
1. Schwarz sagen obwohl kein Grund (kein gutes Blatt, kein außerordentlicher Spielverlauf) vorliegt:  bisher ohne Strafe (nur die ggfs. automatische Bestrafung durch Spielverlust und Extrapunkt(e) bei der Abrechung): [ist noch nicht vorgekommen].
1. Fälschliches, unbegründetes, widerlegbares Einfordern der restlichen Stiche einer Kartenrunde (auch als sogenannter "Heilsarmee Fall" bekannt) durch Offenlegen der eigenen Karten und gierige "gebt mal alles her (Zeichen)sprache": wird bestraft durch den Verlust all dieser restlichen Stiche.
1. Die akkumulierte Maximalstrafe pro Spieler pro Sitzung ist derzeit auf $`\textcolor{red}{\text{\textsf{\$90}}}`$ begrenzt. Minimalstrafe ist $`\textcolor{red}{\text{\textsf{\$0.1}}}`$.
1. Andere Strafen können von den anwesenden Stammspielern per Mehrheitsbeschluss willkürlich festgelegt werden.
1. Alle Punkte im oben gennanten Strafenkatalog können von den anwesenden Stammspielern per Mehrheitsbeschluss spontan geändert werden.
